//
//  SettingsViewController.swift
//  LocationReporter1
//
//  Created by Dan Vybiral on 11/04/16.
//  Copyright © 2016 Dan Vybiral. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData
import HealthKit

class SettingsViewController: UITableViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var locationSwitch: UISwitch!
    @IBOutlet weak var healthKitSwitch: UISwitch!
    @IBOutlet weak var userIdTextField: UITextField!
    @IBOutlet weak var urlTextField: UITextField!
    
    let defaults = NSUserDefaults.standardUserDefaults()
    let healthKitStore:HKHealthStore = HKHealthStore()
    
    lazy var locationManager: CLLocationManager! = {
        let manager = CLLocationManager()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.delegate = self
        manager.requestAlwaysAuthorization()
        return manager
    }()

   
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let locationOn = defaults.boolForKey("locationSwitch")
        let healthOn = defaults.boolForKey("healthKitSwitch")
        
        locationSwitch.setOn(locationOn, animated: true)
        healthKitSwitch.setOn(healthOn, animated: true)
        
        if locationOn {
            startMonitoringLocation()
        } else {
            stopMonitoringLocation()
        }
        
        userIdTextField.text = String(defaults.integerForKey("userId"))
        urlTextField.text = defaults.stringForKey("serverUrl")
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        //self.navigationItem.leftBarButtonItem = self.editButtonItem()
        

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showAlert(text: String) {
        let alertController = UIAlertController(title: "Watcher", message:
            text, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    @IBAction func locationSwitchValueChanged(sender: UISwitch) {
        if sender.on {
            defaults.setBool(true, forKey: "locationSwitch")
            NSLog("Starting to report locations.")
            startMonitoringLocation()
        } else {
            defaults.setBool(false, forKey: "locationSwitch")
            NSLog("Stopping to report locations.")
            stopMonitoringLocation()
        }
    }
    
    @IBAction func healthKitSwitchValueChanged(sender: UISwitch) {
        if sender.on {
            defaults.setBool(true, forKey: "healthKitSwitch")
            
            self.authorizeHealthKit { (authorized,  error) -> Void in
                if authorized {
                    self.showAlert("HealthKit authorization received.")
                }
                else
                {
                    self.showAlert("HealthKit authorization denied!")
                    if error != nil {
                        self.showAlert("\(error)")
                    }
                }
            }
        } else {
            defaults.setBool(false, forKey: "healthKitSwitch")
        }
    }
    
    @IBAction func userIdTextFieldChanged(sender: UITextField) {
        defaults.setInteger(Int(sender.text!)!, forKey: "userId")
    }
    
    @IBAction func serverApiUrlTextFieldChanged(sender: UITextField) {
        defaults.setObject(sender.text, forKey: "serverUrl")
    }
    
    func startMonitoringLocation() {
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startMonitoringSignificantLocationChanges()
        defaults.setBool(true, forKey: "locationMonitoringRunning")
    }
    
    func stopMonitoringLocation() {
        locationManager.stopMonitoringSignificantLocationChanges()
        defaults.setBool(false, forKey: "locationMonitoringRunning")
    }
    
    func addLocation(newLocation: CLLocation) {
        
        // Delegat aplikace, drzici instanci Managed Object Contextu
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        
        // Instance Managed Object Contextu
        let moc = appDelegate.managedObjectContext
        
        // Pozadavek na vytvoreni nove, prazdne, entity konkretniho typu
        let entity = NSEntityDescription.insertNewObjectForEntityForName("Entry", inManagedObjectContext: moc) as! Entry
        
        // Nastaveni konkretnich dat entite
        entity.setValue(defaults.integerForKey("userId"), forKey: "user")
        entity.setValue(NSDate().timeIntervalSince1970, forKey: "timestamp")
        entity.setValue("location", forKey: "type")
        let coordinateString = String(format:"%f", newLocation.coordinate.latitude) + "," + String(format:"%f", newLocation.coordinate.longitude)
        entity.setValue(coordinateString, forKey: "value")
        entity.setValue(false, forKey: "synced")
        
        // Do teto chvile byla nova entita pouze v pameti, zde se jiz ulozi do databaze
        do {
            try moc.save()
        } catch {
            fatalError("Failure to save context: \(error)")
        }
    }
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        if UIApplication.sharedApplication().applicationState == .Active {
            NSLog("App is in foreground. New location is %@", newLocation)
        } else {
            NSLog("App is backgrounded. New location is %@", newLocation)
            addLocation(newLocation)
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if UIApplication.sharedApplication().applicationState == .Active {
            for location in locations {
                NSLog("App is in foreground. Stored location is %@", location)
                addLocation(location)
            }
            
        } else {
            for location in locations {
                NSLog("App is in background. Stored location is %@", location)
                addLocation(location)
            }
        }
    }
    
    
    // MARK - HealthKit access
    
    func authorizeHealthKit(completion: ((success:Bool, error:NSError!) -> Void)!)
    {
        // Mnozina typu zaznamu, ke kterym aplikace vyzaduje opravneni
        let healthKitTypesToRead = Set(arrayLiteral:
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!,
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)!
            )
        
        /*
        Pokud by aplikace vyzadovala opravneni k zapisu do HealhKit uloziste, typy by se definovaly zde
        let healthKitTypesToWrite = Set(arrayLiteral:[
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierBodyMassIndex),
            HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierActiveEnergyBurned)
            ])
        */
        
        // Pokud uzivatel neudeli opravneni, nebo zarizeni nepodporuje HealhKit, je osetrena vyjimka
        if !HKHealthStore.isHealthDataAvailable()
        {
            let error = NSError(domain: "mendelu.locationreporter", code: 2, userInfo: [NSLocalizedDescriptionKey:"HealthKit is not available on this Device"])
            if( completion != nil )
            {
                completion(success:false, error:error)
            }
            return;
        }
        
        // Samotny pozadavek na udeleni opravneni
        healthKitStore.requestAuthorizationToShareTypes(Set(), readTypes: healthKitTypesToRead) { (success, error) -> Void in
            
            if( completion != nil )
            {
                completion(success:success,error:error)
            }
        }
    }


    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 2
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var selectedCell = 0
        
        switch indexPath.section {
        case 0:
            NSLog("You selected cell number: \(indexPath.row)!")
            selectedCell = indexPath.row
            break
        case 1:
            NSLog("You selected cell number: \(indexPath.row+2)!")
            selectedCell = indexPath.row+2
            break
        default:
            break
        }
        
        switch selectedCell {
        case 2:
            userIdTextField.becomeFirstResponder()
        case 3:
            urlTextField.becomeFirstResponder()
        default:
            break
        }
    }

    /*
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("reuseIdentifier", forIndexPath: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
