//
//  FirstViewController.swift
//  LocationReporter1
//
//  Created by Dan Vybiral on 11/04/16.
//  Copyright © 2016 Dan Vybiral. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation

class HomeViewController: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet weak var statusImageView: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    
    let okImage: UIImage = UIImage(named: "status-ok")!
    let warningImage: UIImage = UIImage(named: "status-warning")!
    let errorImage: UIImage = UIImage(named: "status-err")!
    let syncingImage: UIImage = UIImage(named: "syncing")!
    
    let defaults = NSUserDefaults.standardUserDefaults()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if defaults.boolForKey("syncing") {
            statusImageView.image = syncingImage
            statusLabel.text = "Now syncing"
        }
        else if defaults.boolForKey("errors") {
            statusImageView.image = errorImage
            statusLabel.text = "Error: " + defaults.stringForKey("errorMessage")!
        } else if defaults.boolForKey("locationMonitoringRunning") {
            statusImageView.image = okImage
            statusLabel.text = "Everything is OK"
        } else {
            statusImageView.image = warningImage
            statusLabel.text = "Location reporting OFF"
        }
    
    }
        
    func addLocation(newLocation: CLLocation) {
        
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        
        let moc = appDelegate.managedObjectContext
        
        // we set up our entity by selecting the entity and context that we're targeting
        let entity = NSEntityDescription.insertNewObjectForEntityForName("Entry", inManagedObjectContext: moc) as! Entry
        
        // add our data
        entity.setValue(1, forKey: "user")
        entity.setValue(NSDate().timeIntervalSince1970, forKey: "timestamp")
        entity.setValue("location", forKey: "type")
        let coordinateString = String(format:"%f", newLocation.coordinate.latitude) + "," + String(format:"%f", newLocation.coordinate.longitude)
        entity.setValue(coordinateString, forKey: "value")
        entity.setValue(false, forKey: "synced")
        
        // we save our entity
        do {
            try moc.save()
        } catch {
            fatalError("Failure to save context: \(error)")
        }
    }
    
    // MARK: - CLLocationManagerDelegate
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        if UIApplication.sharedApplication().applicationState == .Active {
            NSLog("App is in foreground. New location is %@", newLocation)
        } else {
            NSLog("App is backgrounded. New location is %@", newLocation)
            addLocation(newLocation)
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if UIApplication.sharedApplication().applicationState == .Active {
            for location in locations {
                NSLog("App is in foreground. Stored location is %@", location)
            }
            
        } else {
            for location in locations {
                NSLog("App is in background. Stored location is %@", location)
                addLocation(location)
            }
        }
    }
}

