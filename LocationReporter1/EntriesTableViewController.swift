//
//  EntriesTableViewController.swift
//  LocationReporter1
//
//  Created by Dan Vybiral on 11/04/16.
//  Copyright © 2016 Dan Vybiral. All rights reserved.
//

import UIKit
import CoreData
import HealthKit

class EntriesTableViewController: UITableViewController  {

    @IBOutlet weak var entryTable: UITableView!
    
    var managedObjectContext: NSManagedObjectContext!
    let healthKitStore:HKHealthStore = HKHealthStore()
    
    var entries = [NSManagedObject]()
    let circleX: UIImage = UIImage(named: "phone")!
    let circleTick: UIImage = UIImage(named: "cloud")!
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    var responseString: String = ""
    
    var cancelSync = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.leftBarButtonItem = self.editButtonItem()
        
        /*
        self.entryTable.registerClass(UITableViewCell.self,
                                forCellReuseIdentifier: "EntryCell")*/
        
        let rightItem:UIBarButtonItem = UIBarButtonItem(title: "Actions", style: UIBarButtonItemStyle.Plain, target: self, action: #selector(EntriesTableViewController.syncButtonPressed(_:)))
        
        self.navigationItem.rightBarButtonItem = rightItem
        
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        
        self.managedObjectContext = appDelegate.managedObjectContext
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        /*
        //1
        let managedContext = DataController().managedObjectContext
        
        //2
        let fetchRequest = NSFetchRequest(entityName: "Entry")
        
        //3
        do {
            let results =
                try managedContext.executeFetchRequest(fetchRequest)
            self.entries = results as! [NSManagedObject]
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
         */
        
        /*
        //1
        let appDelegate =
            UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext
        
        //2
        let fetchRequest = NSFetchRequest(entityName: "Entry")
        
        let fetchRequest = NSFetchRequest(entityName: "Entry")
        //3
        do {
            let results =
                try self.managedObjectContext.executeFetchRequest(fetchRequest)
            entries = results as! [NSManagedObject]
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
         */
        
        var firstRun = false
        
        if (defaults.boolForKey("healthKitSwitch")) {
            firstRun = self.defaults.boolForKey("firstRun")
            
            if firstRun {
                self.showAlert("This is a first run. The initial import of HealthKit data will take some time. After the import finishes, you will see the data in this table. Until then it will be probably empty.", completion: {self.getAllDataFromHealthKit()})
                
                self.defaults.setValue(false, forKey: "firstRun")
            } else {
                getLatestDataFromHealthKit()
            }
        }
        
        reloadArrayForTable()
        entryTable.reloadData()
    }
    
    func reloadArrayForTable() {
        let fetchRequest = NSFetchRequest(entityName: "Entry")
        let primarySortDescriptor = NSSortDescriptor(key: "timestamp", ascending: false)
        fetchRequest.sortDescriptors = [primarySortDescriptor]

        do {
            let results =
                try self.managedObjectContext.executeFetchRequest(fetchRequest)
            entries = results as! [NSManagedObject]
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
    }
    
    
    func syncButtonPressed(sender: AnyObject?) {
        // 1
        let optionMenu = UIAlertController(title: nil, message: "Choose action", preferredStyle: .ActionSheet)
        
        // 2
        let syncAction1 = UIAlertAction(title: "Sync only unsynced entries to server", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.defaults.setBool(true, forKey: "syncing")
            self.syncEntries(0, overrideAll: false, cancelled: false)
        })
        let syncAction2 = UIAlertAction(title: "Sync ALL entries to server", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.defaults.setBool(true, forKey: "syncing")
            self.syncEntries(0, overrideAll: true, cancelled: false)
        })
        let healthKitAction = UIAlertAction(title: "Import all data from HealhtKit", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.getAllDataFromHealthKit()
        })
        
        //
        let cancelSyncAction = UIAlertAction(title: "Cancel sync", style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.cancelSync = true
            self.defaults.setBool(false, forKey: "syncing")
            
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
            
        })
        
        
        // 4
        optionMenu.addAction(syncAction1)
        optionMenu.addAction(syncAction2)
        optionMenu.addAction(healthKitAction)
        optionMenu.addAction(cancelSyncAction)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.presentViewController(optionMenu, animated: true, completion: {
            self.reloadArrayForTable()
            self.entryTable.reloadData()
        })
    }
    
    func showAlert(text: String) {
        let alertController = UIAlertController(title: "Watcher", message:
            text, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func showAlert(text: String, completion: (() -> Void)!) {
        let alertController = UIAlertController(title: "Watcher", message:
            text, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: completion)
    }
    
    func syncEntries(fromEntryIndex: Int, overrideAll: Bool, cancelled: Bool) {
        if self.cancelSync {
            self.showAlert("All entries are synced.")
            do {
                try self.managedObjectContext.save()
            } catch let error as NSError {
                print("Error in updating the entity: \(error)")
            }
            self.defaults.setBool(false, forKey: "syncing")
            self.reloadArrayForTable()
            self.entryTable.reloadData()
            self.cancelSync = false
            return
        }
        
        let entryCount = self.entries.count
        let entrySynced: Bool = self.entries[fromEntryIndex].valueForKey("synced") as! Bool
        
        if !overrideAll && entrySynced {
            if fromEntryIndex < self.entries.endIndex-1 {
                self.syncEntries(fromEntryIndex+1, overrideAll: overrideAll, cancelled: cancelled)
            } else {
                self.showAlert("All entries are synced.")
                do {
                    try self.managedObjectContext.save()
                } catch let error as NSError {
                    print("Error in updating the entity: \(error)")
                }
                
                self.defaults.setBool(false, forKey: "syncing")
                self.reloadArrayForTable()
                self.entryTable.reloadData()
                self.cancelSync = false
            }
        } else {
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            
            let url: String = defaults.stringForKey("serverUrl")!
            let request = NSMutableURLRequest(URL: NSURL(string: url)!)
            request.HTTPMethod = "POST"
            
            
            var postString: String = "user=" + defaults.stringForKey("userId")!

            //set(['entry_datetime', 'value', 'value_type', 'user', 'device']
            
            postString += "&value_type=" + String(self.entries[fromEntryIndex].valueForKey("type"))
            postString += "&value=" + String(self.entries[fromEntryIndex].valueForKey("value"))
            postString += "&device=1"
            postString += "&entry_datetime=" + String(self.entries[fromEntryIndex].valueForKey("timestamp"))
            
            request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
            let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(error)")
                    self.responseString = String(error)
                    self.defaults.setValue(true, forKey: "errors")
                    self.defaults.setValue("network error", forKey: "errorMessage")
                    self.performSegueWithIdentifier("responseViewSegue", sender: request)
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    self.defaults.setBool(false, forKey: "syncing")
                    return
                }
                
                if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 201 {           // check for http errors
                    print("statusCode should be 201, but is \(httpStatus.statusCode)")
                    print("response = \(response)")
                    let responseStringLocal = NSString(data: data!, encoding: NSUTF8StringEncoding)
                    print("responseString = \(responseStringLocal)")
                    self.defaults.setValue(true, forKey: "errors")
                    self.defaults.setValue("server responded with error", forKey: "errorMessage")
                    self.performSegueWithIdentifier("responseViewSegue", sender: request)
                    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                    self.defaults.setBool(false, forKey: "syncing")
                    return
                }
                
                // TODO Duplicate code, remove later
                let responseStringLocal = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print("responseString = \(responseStringLocal)")
                self.responseString = String(responseStringLocal)
                
                // Here the entry was already synced, co we can change the entity
                self.defaults.setValue(false, forKey: "errors")
                self.defaults.setValue("none", forKey: "errorMessage")
                self.entries[fromEntryIndex].setValue(true, forKey: "synced")
                
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                
                // Call recursively until the last entry is synced. This is because this is called after the current call is completed and
                // the app received response from server, so the app doesnt send many requests at once.
                if fromEntryIndex < self.entries.endIndex-1 {
                    self.syncEntries(fromEntryIndex+1, overrideAll: overrideAll, cancelled: cancelled)
                } else {
                    self.showAlert("All entries are synced.")
                    
                    do {
                        try self.managedObjectContext.save()
                    } catch let error as NSError {
                        print("Error in updating the entity: \(error)")
                    }
                    
                    self.defaults.setBool(false, forKey: "syncing")
                    self.reloadArrayForTable()
                    self.entryTable.reloadData()
                    self.cancelSync = false
                }
            }
            
            task.resume()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - HealthKit access
    
    func addHealthEntry(qunatitySample: HKQuantitySample) {
        // we set up our entity by selecting the entity and context that we're targeting
        
        if qunatitySample.quantityType == HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate) {
            let entity = NSEntityDescription.insertNewObjectForEntityForName("Entry", inManagedObjectContext: self.managedObjectContext) as! Entry
            
            let sampleStartDate = qunatitySample.startDate.timeIntervalSince1970
            
            entity.setValue(defaults.valueForKey("userId"), forKey: "user")
            entity.setValue(sampleStartDate, forKey: "timestamp")
            entity.setValue("heartrate", forKey: "type")
            let heartRateValue = String(format:"%f", qunatitySample.quantity.doubleValueForUnit(HKUnit.countUnit().unitDividedByUnit(HKUnit.minuteUnit())))
            entity.setValue(heartRateValue, forKey: "value")
            entity.setValue(false, forKey: "synced")
            
            // Save must be called after all the entries are added, so only after the big for in other functions is finished.
            /*
            do {
                try self.managedObjectContext.save()
            } catch {
                fatalError("Failure to save context: \(error)")
            }
             */
        }
        
        if qunatitySample.quantityType == HKQuantityType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount) {
            let entity = NSEntityDescription.insertNewObjectForEntityForName("Entry", inManagedObjectContext: self.managedObjectContext) as! Entry
            
            let sampleStartDate = qunatitySample.startDate.timeIntervalSince1970
            
            entity.setValue(defaults.valueForKey("userId"), forKey: "user")
            entity.setValue(sampleStartDate, forKey: "timestamp")
            entity.setValue("stepcount", forKey: "type")
            let stepCountValue = String(format:"%f", qunatitySample.quantity.doubleValueForUnit(HKUnit.countUnit()))
            entity.setValue(stepCountValue, forKey: "value")
            entity.setValue(false, forKey: "synced")
            
            // Save must be called after all the entries are added, so only after the big for in other functions is finished.
            /*
            do {
                try self.managedObjectContext.save()
            } catch {
                fatalError("Failure to save context: \(error)")
            }
            */
        }
    }
    
    func getLatestDataFromHealthKit() {
        // Casova razitka pro novy dotaz - nechceme vsechny polozky, ale pouze z casoveho intervalu
        let past = defaults.valueForKey("lastHealthSync") as! NSDate
        let now   = NSDate()
        // Casovy predikat pro dotaz
        let mostRecentPredicate = HKQuery.predicateForSamplesWithStartDate(past, endDate:now, options: .None)
        
        //  Deskriptor zabezpecujici serazenost vracenych vysledku od nejnovejsiho po nejstarsi
        let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: false)
        
        //  Samotny dotaz, obsahujuci definici typu (tep), predikat, deskriptor a anonymni funkci, ktera vracena data zpracuje
        let sampleQuery = HKSampleQuery(sampleType: HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!, predicate: mostRecentPredicate, limit: HKObjectQueryNoLimit, sortDescriptors: [sortDescriptor]) { (sampleQuery, results, error ) -> Void in
            
            if error != nil {
                self.showAlert("HealthKit failed to provide new data.")
                return
            }
            
            // Pokud nenastala chyba, vsechny polozky se zpracuji
            for sample in results! {
                let qunatitySample = sample as! HKQuantitySample
                self.addHealthEntry(qunatitySample)
            }
            
            // Save the last time of the sync, so already processed samples won't get queried in the future
            self.defaults.setValue(NSDate(), forKey: "lastHealthSync")
            
            do {
                try self.managedObjectContext.save()
            } catch {
                fatalError("Failure to save context: \(error)")
            }
            
            self.reloadArrayForTable()
            self.entryTable.reloadData()
        }
        
        //  Build samples query for stepcount
        let sampleQuery2 = HKSampleQuery(sampleType: HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)!, predicate: mostRecentPredicate, limit: HKObjectQueryNoLimit, sortDescriptors: [sortDescriptor]) { (sampleQuery, results, error ) -> Void in
            
            if error != nil {
                self.showAlert("HealthKit failed to provide new data.")
                return
            }
            
            // Process returned samples
            for sample in results! {
                let qunatitySample = sample as! HKQuantitySample
                self.addHealthEntry(qunatitySample)
            }
            
            // Save the last time of the sync, so already processed samples won't get queried in the future
            self.defaults.setValue(NSDate(), forKey: "lastHealthSync")
            
            do {
                try self.managedObjectContext.save()
            } catch {
                fatalError("Failure to save context: \(error)")
            }
            
            self.reloadArrayForTable()
            self.entryTable.reloadData()
        }
        
        // Execute the Query
        self.healthKitStore.executeQuery(sampleQuery)
        // Execute the Query
        self.healthKitStore.executeQuery(sampleQuery2)
    }
    
    func getAllDataFromHealthKit() {
        let past = NSDate.distantPast()
        let now   = NSDate()
        let mostRecentPredicate = HKQuery.predicateForSamplesWithStartDate(past, endDate:now, options: .None)
        
        //  Build the sort descriptor to return the samples in descending order
        let sortDescriptor = NSSortDescriptor(key:HKSampleSortIdentifierStartDate, ascending: false)
        //  we want to limit the number of samples returned by the query to just 1 (the most recent)
        //let limit = 1
        
        //  Build samples query
        let sampleQuery = HKSampleQuery(sampleType: HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierHeartRate)!, predicate: mostRecentPredicate, limit: HKObjectQueryNoLimit, sortDescriptors: [sortDescriptor]) { (sampleQuery, results, error ) -> Void in
            
            if error != nil {
                self.showAlert("HealthKit failed to provide new data.")
                return
            }
            
            // Process returned samples
            for sample in results! {
                let qunatitySample = sample as! HKQuantitySample
                self.addHealthEntry(qunatitySample)
            }
            
            // Save the last time of the sync, so already processed samples won't get queried in the future
            self.defaults.setValue(NSDate(), forKey: "lastHealthSync")
            
            do {
                try self.managedObjectContext.save()
            } catch {
                fatalError("Failure to save context: \(error)")
            }
        }
        
        //  Build samples query for stepcount
        let sampleQuery2 = HKSampleQuery(sampleType: HKObjectType.quantityTypeForIdentifier(HKQuantityTypeIdentifierStepCount)!, predicate: mostRecentPredicate, limit: HKObjectQueryNoLimit, sortDescriptors: [sortDescriptor]) { (sampleQuery, results, error ) -> Void in
            
            if error != nil {
                self.showAlert("HealthKit failed to provide new data.")
                return
            }
            
            // Process returned samples
            for sample in results! {
                let qunatitySample = sample as! HKQuantitySample
                self.addHealthEntry(qunatitySample)
            }
            
            // Save the last time of the sync, so already processed samples won't get queried in the future
            self.defaults.setValue(NSDate(), forKey: "lastHealthSync")
            self.defaults.setValue(false, forKey: "firstRun")
            
            do {
                try self.managedObjectContext.save()
            } catch {
                fatalError("Failure to save context: \(error)")
            }
            
            self.reloadArrayForTable()
            self.entryTable.reloadData()
        }
        
        // Execute the Query
        self.healthKitStore.executeQuery(sampleQuery)
        // Execute the Query
        self.healthKitStore.executeQuery(sampleQuery2)
    }
    

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return entries.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("EntryCell", forIndexPath: indexPath) as! EntryCell
        
        let entry = entries[indexPath.row]
        let entrySynced: Bool = entry.valueForKey("synced") as! Bool
        
        if entrySynced {
            cell.statusImage.image = self.circleTick
        } else {
            cell.statusImage.image = self.circleX
        }
        
        cell.headerLabel!.text = entry.valueForKey("type") as? String
        cell.detailLabel!.text = entry.valueForKey("value") as? String
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            self.entries.removeAtIndex(indexPath.row)
            self.entryTable.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            
            /*
            //1
            let appDelegate =
                UIApplication.sharedApplication().delegate as! AppDelegate
            
            let managedContext = appDelegate.managedObjectContext
            */
            //2
            let fetchRequest = NSFetchRequest(entityName: "Entry")
            
            //3
            do {
                var results = try self.managedObjectContext.executeFetchRequest(fetchRequest)
                entries = results as! [NSManagedObject]
                
                self.managedObjectContext.deleteObject(results[indexPath.row] as! NSManagedObject)
                results.removeAtIndex(indexPath.row)
                try self.managedObjectContext.save()
                
                self.reloadArrayForTable()
                self.entryTable.reloadData()
                
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
            }
        }
        
        
        /*
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
        */
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.performSegueWithIdentifier("entryDetailSegue", sender: indexPath)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if (segue.identifier == "responseViewSegue") {
            let nextNavigationController = (segue.destinationViewController as! UINavigationController)
            let nextViewController = nextNavigationController.viewControllers[0] as! ResponseViewController
            nextViewController.pageToLoad = self.responseString
        }
        
        if (segue.identifier == "entryDetailSegue") {
            let indexPath = sender as! NSIndexPath
            let entry = entries[indexPath.row]
            let nextNavigationController = (segue.destinationViewController as! UINavigationController)
            let nextViewController = nextNavigationController.viewControllers[0] as! EntryDetailViewController
            
            let formatter = NSDateFormatter()
            formatter.dateFormat = "yyyy-MM-dd' 'HH:mm"
            formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
            let timeString = formatter.stringFromDate(NSDate(timeIntervalSince1970: Double(entry.valueForKey("timestamp") as! Double)))
            
                // Push data
            nextViewController.entryType = entry.valueForKey("type") as! String
            nextViewController.timestamp = timeString
            nextViewController.value = entry.valueForKey("value") as! String
            nextViewController.user = String(entry.valueForKey("user"))
            
            let synced = entry.valueForKey("synced") as! Bool
            
            if synced {
                nextViewController.sync = "entry synced"
            } else {
                nextViewController.sync = "entry not synced"
            }
        }
    }
    
    @IBAction func responseViewDone(segue:UIStoryboardSegue) {
        
    }

}
