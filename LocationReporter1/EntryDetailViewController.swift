//
//  EntryDetailViewController.swift
//  LocationReporter1
//
//  Created by Dan Vybiral on 14/04/16.
//  Copyright © 2016 Dan Vybiral. All rights reserved.
//

import UIKit

class EntryDetailViewController: UIViewController {
    
    @IBOutlet weak var entryTypeLabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var userLabel: UILabel!
    @IBOutlet weak var syncLabel: UILabel!
    
    var entryType = String()
    var timestamp = String()
    var value = String()
    var user = String()
    var sync = String()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        entryTypeLabel.text = entryType
        timestampLabel.text = timestamp
        valueLabel.text = value
        userLabel.text = user
        syncLabel.text = sync
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
