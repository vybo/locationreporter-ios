//
//  ResponseViewController.swift
//  LocationReporter1
//
//  Created by Dan Vybiral on 13/04/16.
//  Copyright © 2016 Dan Vybiral. All rights reserved.
//

import UIKit

class ResponseViewController: UIViewController {
    @IBOutlet weak var webView: UIWebView!
    
    var pageToLoad: String = "Nothing to show."

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        webView.loadHTMLString(pageToLoad, baseURL: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func newPageToLoad(page: String) {
        self.pageToLoad = page
        webView.loadHTMLString(pageToLoad, baseURL: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
