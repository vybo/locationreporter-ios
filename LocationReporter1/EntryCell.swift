//
//  EntryCell.swift
//  LocationReporter1
//
//  Created by Dan Vybiral on 11/04/16.
//  Copyright © 2016 Dan Vybiral. All rights reserved.
//

import UIKit

class EntryCell: UITableViewCell {
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var statusImage: UIImageView!
    
    /*
    var entry: Entry! {
        didSet {
            headerLabel.text = entry.type
            detailLabel.text = entry.value
        }
    }
 */

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
