//
//  Entry.swift
//  LocationReporter1
//
//  Created by Dan Vybiral on 11/04/16.
//  Copyright © 2016 Dan Vybiral. All rights reserved.
//

import Foundation
import CoreData

class Entry: NSManagedObject {
    @NSManaged var type: String?
    @NSManaged var timestamp: Double
    @NSManaged var value: String?
    @NSManaged var user: Int8
    @NSManaged var synced: Bool
}